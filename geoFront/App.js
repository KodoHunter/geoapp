import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

setInterval(function(){
  fetch('localhost:1337/location', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      lat: '60.984271', // pitää hakea gps sijainti tähän
      long: '25.661050', // esiasetettuna Jarkon Metalli
    })
  }) // kun vastaus saadaan, vaihdetaan tila sen mukaiseksi
  // eli kohteen/kohteiden lähellä, tai tyhjä
  .then((response) => response.json())
  .then((responseJson) => {
    console.log(responseJson.msgtitle);
    console.log(responseJson.msg);
  })
  .catch((error) => {
    console.error(error);
  });
}, 5000);


export default class App extends React.Component {
  render() {
    return (
      /*
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <Text>Changes you make will automatically reload.</Text>
        <Text>Shake your phone to open the developer menu.</Text>
      </View>
      */
      <View style={styles.container}>
        <View style={{flex: 4, backgroundColor: 'powderblue'}} />
        <View style={{flex: 1, backgroundColor: 'steelblue'}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  somethingElse: {
    flex: 9001,
    backgroundColor: '#000',
  }
});
