/**
 * location.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    title: {
      type: 'string',
      required: true,
      defaultsTo: ''
    },
    content: {
      type: 'string',
      defaultsTo: ''
    },
    lat: {
      type: 'string',
      required: true
    },
    long: {
      type: 'string',
      required: true
    },
    sear: {
      type: 'int',
      required: true,
      defaultsTo: 1
    },
    disabled: {
      type: 'boolean',
      required: true,
      defaultsTo: false
    }
  }
};
