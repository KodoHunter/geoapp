/**
 * usersController
 *
 * @description :: Handles user management
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  // create a new user
  register: function(req, res){

    var usrn = req.body.username;
    var pass = req.body.password;

    console.log(usrn.length);

    // make sure both are at least 4 chars long
    // empty input breaks the program.
    if( usrn > 4 && pass > 4){

      users.find({
        username: usrn
      }).then(function (entries){
        if(!entries[0]){ //if no entry exists, create new

          users.create({
            username: usrn,
            password: pass,
            auth: 0,

          }).exec(function (err, newUsr){
            if (err) { return res.serverError(err); }

            console.log("User added:");
            console.log(newUsr);
            // Inform the user of success
            return res.send("User "+newUsr.username+" added succesfully");
          })

        } else {
          console.log("Username is already taken!");
          return res.send("Username is already taken!");
        }

      })
      // if either username or password is too short, notify about it and abort registration
    } else {
      var reErrMsg;
      if(usrn < 4){
        reErrMsg += ("Username must be at least 4 characters.\n");
      }
      if(pass < 4){ // will only notify if username is long enough
        reErrMsg += ("Password must be at least 4 characters.");
      }
      res.send(reErrMsg);
    }



  },

  //check login and return auth token
  login: function(req, res){

    var usrn = req.body.username;
    var pass = req.body.password;

    users.find({
      username: usrn
    }).then(function (entries){

      if(entries[0]){

        if(entries[0].password == pass){
          console.log(usrn + " has logged in");
          return res.send("Successfully logged in as "+usrn);
        } else {
          console.log("Invalid password.");
          return res.send("Invalid password.");
        }

      } else {
        console.log("Username not found.");
        return res.send("Username not found.");
      }

    })
  },
  // Removing a user
  delete: function(req, res){

    var usrn = req.body.username;
    var pass = req.body.password;

    users.find({
      username: usrn
    }).then(function (entries){

      if(entries[0]){

        if(entries[0].password == pass){
          users.destroy({ username:usrn })
          .exec(function(err){
            if(err) {
              return res.negotiate(err);
            }
            return res.send("Successfully removed user "+usrn+".");
          });


        } else {
          return res.send("Invalid password.");
        }

      } else {
        return res.send("Username not found.");
      }

    })
  }

};
