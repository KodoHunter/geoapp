/**
 * locationController
 *
 * @description :: Server-side logic for checking location
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
 function realDistance(dbLat, dbLong, inLat, inLong){

   // math. Returns the actual distance between points
   // pretty accurate.
   Number.prototype.toRad = function() {
     return this * Math.PI / 180;
   }

   var R = 6371e3;
   var f1 = dbLat.toRad(), l1 = dbLong.toRad();
   var f2 = inLat.toRad(), l2 = inLong.toRad();
   var df = f2 - f1;
   var dl = l2 - l1;

   var a = Math.sin(df/2) * Math.sin(df/2)
         + Math.cos(f1) * Math.cos(f2)
         * Math.sin(dl/2) * Math.sin(dl/2);
   var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
   var d = R * c;

   return d;
 }



module.exports = {

  locationCheck: function (req, res) {
    // Prepare most variables
    var count = 0;
    var found_bool = false;
    var sent = false;
    var sendTitle = [];
    var sendContent = [];
    var howManyFound = 0;

    // Find out how many locations are in DB
    location.count().exec(function (error, found) {
      if(error) {
        console.log("Error occurred in .count");
        return res.send("Error occurred in .count");
      }

      console.log("found: "+found);
      count = found;
      advance(); // Asynchronity reasons, only continue after ready
    })

    function advance(){
      console.log("got: "+count);

        // Search for the DB items. Sear is always 1, maybe not best solution?
        location.find({
          sear: 1
        }).then( function (entries) {

          // Go trough all the found items
          for(var x = 0; x < (count); x++){
            var dbLat;
            var dbLong;
            var inLat = 0;
            var inLong = 0;

            // Check if the item's disable flag is set
            if(!entries[x].disabled){

              console.log(" ");
              //console.log(entries[x].disabled);

              console.log("before x: "+x);

              // Need to parse, as location data is a string in DB
              // This is because somewhere along the way, Float got rounded
              dbLat = parseFloat(entries[x].lat);
              dbLong = parseFloat(entries[x].long);

              inLat = req.body.lat;
              inLong = req.body.long;

              // use the math func to find out distance
              var distance = realDistance(dbLat, dbLong, inLat, inLong);
              console.log("Distance to "+ entries[x].title + ": " + distance);

              // if distance meets condition, save as part of return item
                if(distance < 10){
                  // Create new arrays, with incrementing index
                  sendTitle[howManyFound] = entries[x].title
                  sendContent[howManyFound] = entries[x].content
                  howManyFound++;
                  found_bool = true; // set found to true. Should never be set to false again.
                }/* else {
                  // no need to ever set false
                  found_bool = false;
                }*/
              console.log("count: "+count);
              console.log("X: "+x);

              // If we're done, found any or not, send results
                if(!found_bool && x == count -1 && !sent){
                  console.log("No match found");
                  res.send("No match found");
                  sent = true;
                }
                if(found_bool && x == count -1 && !sent){
                  console.log("Match(es) found");
                  sent = true;

                  // Prepare a return object
                  var retObj = [];

                  // For each item found, add their array points to an object
                  for(var y = 0; y < howManyFound; y++){
                    var obj = {
                      msgtitle: sendTitle[y],
                      msg: sendContent[y]
                    }
                    // add the object to the return object before moving the next item
                    retObj.push(obj);
                  }
                  // send the result object
                  res.send(retObj);

                } // if found end

              } else {
                // found_bool = false;

                // Last db item was disabled, but we check if we're done anyways
                // If we didn't find anything
                if(!found_bool && x == count -1 && !sent){
                  console.log("No match found");
                  res.send("No match found");
                  sent = true;
                }
                if(found_bool && x == count -1 && !sent){
                  console.log("Match(es) found");
                  sent = true;

                  var retObj = [];

                  for(var y = 0; y < howManyFound; y++){
                    var obj = {
                      msgtitle: sendTitle[y],
                      msg: sendContent[y]
                    }
                    retObj.push(obj);
                  }

                  res.send(retObj);

                } // if found end

              }
          } // for loop end
        })// location.find end
        .catch( function (err) {
          console.log("An error occurred in .find" + err);
          return res.send("An error occurred in .find");
        });

    } // advance end

    return console.log("Gotcha");

  }, // locationCheck end

  // function for adding locations to database
  addLocation: function(req, res){

    // prepare required variables
    var senLat = req.body.lat;
    var senLong = req.body.long;
    var senSear = 1;
    var senTitle = req.body.title;
    var senContent = req.body.content;

    // make sure coordinates are in correct format
    if(!isNaN(senLat) && senLat.toString().indexOf('.') != -1 && !isNaN(senLong) && senLlong.toString().indexOf('.') != -1 ){
      location.create({
        title: senTitle,
        content: senContent,
        lat: senLat,
        long: senLong,
        sear: senSear
      }).exec(function (err, newLoc){
        if (err) { return res.serverError(err); }

        console.log(newLoc);
        return res.send("Location added to database succesfully.");
      })
    }
    else {
      console.log("fail");
      return res.send("An error occurred while adding location to database.");
    }

  }, // addLocation end

  setDisable: function(req, res){
    var tid = req.body.id;
    var cid = Math.round(req.body.id);
    var dStatus = req.body.value;
    var targetTitle = req.body.title;


    console.log(typeof targetTitle);
    console.log(typeof tid);
    console.log(tid);
    // remove by ID or title? ID has priority
    if (tid !== undefined) {
      console.log("Id found, disabling based on that.");

      // Make sure ID is a complete number, and not a fraction (of a human).
      if(tid == cid){

        location.count().exec(function (error, found) {
          if(error) {
            console.log("Error occurred in .count");
            return res.send("Error occurred in .count");
          }
          console.log(found);

          // if given ID is smaller (or equal) than the amount of locs. in db, proceed
          if(found >= tid){
            // update disabled status in given id
            location.update(
              {id: tid},
              {disabled: dStatus}
            ).exec(function(err, updated){

              // invalid values cause database error, so err triggers
              // database is no updated in this case
              if(err) {
                console.log("Update failed");
                return res.send("An error occurred with disable. Make sure the value is 0 or 1");
              }

              // If success, inform what was done
              console.log(updated);
              if(dStatus == 0){
                return res.send("Location activated succesfully.");
              } else {
                return res.send("Location disabled succesfully");
              }
            })
            // if none found
          } else {
            return res.send("No location was found with that ID");
          }

        }) // end of location.count

      } else { //if tid == cid -statement end
        return res.send("Check the inputted location ID");
      }

    } else if (targetTitle !== undefined) {
      console.log("Title found, checking title...");

      location.find({
        title: targetTitle
      }).exec(function (err, titleHits){
        if (err) {
          return res.serverError(err);
        }
        // console.log(titleHits);

        if(titleHits.length == 0){
          console.log(targetTitle + " was not found in the database.");
          res.send(targetTitle + " was not found in the database.");
        } else {

          location.update(
            {title: targetTitle},
            {disabled: dStatus}
          ).exec(function(err, updated){
            if(err){
              console.log("An error occurred with the server. Please try again.");
              res.send("An error occurred: " + err);
            } else {
              if(dStatus==0){
                console.log(targetTitle + " has been enabled");
                res.send(targetTitle + " has been enabled");
              } else {
                console.log(targetTitle + " has been disabled");
                res.send(targetTitle + " has been disabled");
              }
            }
          })

        }

      });

    } else {
      console.log("Neither ID not Title found.");
      res.send("Invalid target parameters.")
    }

  }

}; // module exports end
