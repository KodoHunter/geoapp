/*
console.log("is anything being done here?");
var sent = false;
while(!sent){

if(done){
  console.log("got to 'done' -if");
  if(!found){
    console.log("no match");
    return res.send("No match found");
  } else {
    console.log("match");
    return res.send({
      title: sendTitle,
      msg: sendContent
    });
  }
  done = false;
  sent = true;
}
}
*/
/**
 * TestController
 *
 * @description :: Server-side logic for managing Tests
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
 function realDistance(dbLat, dbLong, inLat, inLong){

   Number.prototype.toRad = function() {
     return this * Math.PI / 180;
   }

   var R = 6371e3;
   var f1 = dbLat.toRad(),  l1 = dbLong.toRad();
   var f2 = inLat.toRad(), l2 = inLong.toRad();
   var df = f2 - f1;
   var dl = l2 - l1;

   var a = Math.sin(df/2) * Math.sin(df/2)
         + Math.cos(f1) * Math.cos(f2)
         * Math.sin(dl/2) * Math.sin(dl/2);
   var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
   var d = R * c;

   return d;
 }
module.exports = {

	/**
   * `TestController.version()`

  version: function (req, res) {
    console.log("hoo");
    return res.json({
      version: '0.1'
    });
  }

  */



  /*
  -Figure out location array length
  -Loop the location find

  -Figure out how much locational difference minutes have
    -As feared, varies with location
  -Hard compare location lat&long to req +/-10m
  -Return proper results
  */
  locationCheck: function (req, res) {
    //console.log(req.body.title);
    var count = 0;
    var found = false;
    //var done = false;
    var sent = false;
    var sendTitle = [];
    var sendContent = [];
    var howManyFound = 0;
    location.count().exec(function (error, found) {
      console.log("found: "+found);
      count = found;
      advance();
    })

    function advance(){
      console.log("got: "+count);

      for(var x = 0; x < (count); x++){
        var dbLat = 0;
        var dbLong = 0;
        var inLat = 0;
        var inLong = 0;

        console.log("before x: "+x);
        location.find({
          id: (x + 1)
        }).then( function (entries) {
          dbLat = entries[0].lat;
          dbLong = entries[0].long;
          inLat = req.body.lat;
          inLong = req.body.long;
          console.log(" ");
          console.log(inLat);
          console.log(" ");
          console.log(dbLat);
          console.log(" ");

          var distance = realDistance(dbLat, dbLong, inLat, inLong);
          console.log(distance);
          //if((dbLat - 5) < inLat && inLat < (dbLat + 5)){
            //if((dbLong - 5) < inLong && inLong < (dbLong + 5)){
            if(distance < 10){
              sendTitle[howManyFound] = entries[0].title
              sendContent[howManyFound] = entries[0].content
              found = true;
              howManyFound++;
            } else {
              found = false;
            }
            //}
          //}
          console.log("count: "+count);
          console.log("X: "+x);
          //if(!done){
            if(!found && x == count && !sent){
              console.log("No match found");
              // return res.send("hoo");
              sent = true;
            }
            if(found && x == count && !sent){
              console.log("match");
              sent = true;

              for(var y = 0; y < howManyFound; y++){
                console.log({
                  msgtitle: sendTitle[y],
                  msg: sendContent[y]
                });
              }

            }
        })// location.find end
      } // for loop end
    }

return res.send("Gotcha");

  } // locationCheck end

}; // module exports end
