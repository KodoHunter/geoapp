/**
 * TestController
 *
 * @description :: Server-side logic for managing Tests
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	/**
   * `TestController.version()`
   */
  version: function (req, res) {
    console.log("hoo");
    return res.json({
      version: '0.1'
    });
  }

};
